git clone https://aur.archlinux.org/yay.git && cd yay
makepkg -si # You can rm -rf this yay repo after makepkg

sudo pacman -S hyprland waybar wezterm vim neovim thunar rofi ttf-jetbrains-mono-nerd chromium hyprpaper zsh neofetch brightnessctl otf-font-awesome
yay -S wlogout swww pokemon-colorscripts-git oh-my-zsh
